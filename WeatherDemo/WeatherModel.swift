//
//  WeatherModel.swift
//  WeatherDemo
//
//  Created by Vadim Osovets on 8/16/17.
//  Copyright © 2017 Vadim Osovers. All rights reserved.
//

import Foundation

import Moya
import Mapper
import Moya_ModelMapper
import RxOptional
import RxSwift

class WeatherModel {
    
    let provider: RxMoyaProvider<WeatherService>
    let cities: [String]
    
    let weather: Observable<[Weather]>
    var weatherVar = Variable<[Weather]>([])
    
    let disposeBag = DisposeBag()
    
    init (provider: RxMoyaProvider<WeatherService>, cities: [String]) {
        
        self.provider = provider
        self.cities = cities
        
        self.weather = weatherVar.asObservable()
        
        for city in cities {
            
            let addressComponents = city.components(separatedBy: ", ")
            
            guard let cityName = addressComponents.first else { continue }
            
            print ("City: \(cityName)")
            
            _ = provider.request(.current(city: cityName)).mapArrayOptional(type: Weather.self, keyPath: "list")
                .subscribe { event in
                    switch event {
                    case .next(let weather):
                        if let weather = weather {
                            
                            print(weather)
                            
                            self.weatherVar.value.append(contentsOf: weather)
                        }
                    case .error(let error):
                        print(error)
                    default: break
                    }
            }
        }
    }
}
