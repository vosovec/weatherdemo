//
//  WeatherTableViewCell.swift
//  WeatherDemo
//
//  Created by Vadim Osovets on 8/16/17.
//  Copyright © 2017 Vadim Osovers. All rights reserved.
//

import UIKit

import Moya
import RxSwift
import RxCocoa

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var weatherImageView: UIImageView!

    @IBOutlet weak var timeDateLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    let disposeBag = DisposeBag()
    
    var provider: RxMoyaProvider<WeatherIconService>!
    
    func configure(weather: Weather) {
        provider = RxMoyaProvider<WeatherIconService>()
        
        windSpeedLabel.text = "\(weather.wind.speed) meter/sec"
        humidityLabel.text = "\(weather.main.humidity)%"
        
        temperatureLabel.text = "\(weather.main.temp)°C"
        
        timeDateLabel.text = weather.timeDate
        
        if let weatherItem = weather.elements.first {
            provider?.request(.icon(name: weatherItem.icon)).mapImage().subscribe { (event) in
                if let image = event.element {
                    self.weatherImageView.image = image
                }
                }.addDisposableTo(disposeBag)
        }
        
        
    }
}
