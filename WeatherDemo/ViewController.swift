//
//  ViewController.swift
//  WeatherDemo
//
//  Created by Vadim Osovets on 8/15/17.
//  Copyright © 2017 Vadim Osovers. All rights reserved.
//

import UIKit

import RxSwift
import Moya
import RxCocoa

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    
    var cities = [String]()
    
    var provider: RxMoyaProvider<GeoService>!
    var citiesModel: CitiesModel!
    
    var cityName: Observable<String> {
        return searchBar.rx.text
            .orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .filter { !$0.isEmpty }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRx()
    }
    
    func setupRx() {
        
        provider = RxMoyaProvider<GeoService>()
        citiesModel = CitiesModel(provider: provider, cityName: cityName)
        
        citiesModel.cities.bindTo(tableView.rx.items){ (tableView, row, item) in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: IndexPath(row: row, section: 0))
            cell.textLabel?.text = item
            
            return cell
            }
            .addDisposableTo(disposeBag)
        
        // select items
        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self]indexPath in
                
                if let viewController: WeatherViewController = self?.storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as? WeatherViewController {
                    
                    if let city = self?.citiesModel.citiesVar.value[indexPath.item] {
                        viewController.cities = Array(arrayLiteral: city)
                    }
                    
                    self?.navigationController?.pushViewController(viewController, animated: true)
                }
                
            }).addDisposableTo(disposeBag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

