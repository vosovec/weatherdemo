//
//  GeoService.swift
//  WeatherTest
//
//  Created by Vadim Osovets on 8/15/17.
//  Copyright © 2017 Vadim Osovers. All rights reserved.
//

import Foundation
import Moya

enum GeoService {
    case cities(searchQuery: String)
}

// MARK: - TargetType Protocol Implementation
extension GeoService: TargetType {
    var baseURL: URL { return URL(string: "http://gd.geobytes.com")! }
    
    var path: String {
        switch self {
        case .cities( _):
            return "/AutoCompleteCity"
        }
    }
    var method: Moya.Method {
        switch self {
        case .cities:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case .cities(let searchQuery):
            return ["q": searchQuery]
        }
    }
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .cities:
            return URLEncoding.default
        }
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        switch self {
        case .cities:
            return .request
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
